package main;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.controller.*;
import main.model.*;

import javax.xml.bind.*;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

public class Main extends Application {

    private final String title = "Stock Mananger";

    private Stage primaryStage;
    private BorderPane mainView;

    private ObservableList<Product> productData = FXCollections.observableArrayList();

    private ObservableList<ItemStock> itemStockData = FXCollections.observableArrayList();

    private ObservableList<Stock> stockData = FXCollections.observableArrayList();

    private ObservableList<Sale> saleData = FXCollections.observableArrayList();

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(title);
        primaryStage.setResizable(false);

        initMainView();

        setScene(loadLoginView());

        //handleLogin("admin", "admin");
    }

    private void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/icon/icon.png")));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        System.out.println(getClass().getResource("/fxml/MainView.fxml"));
        loader.setLocation(getClass().getResource("/fxml/MainView.fxml"));

        stockData.add(new Stock(1, "Stock1", getItemStockData()));

        mainView = loader.load();

        ((MainController) loader.getController()).setMain(this);

    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/LoginView_GRID.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadProductOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ProductOverviewView.fxml"));

        Parent root = loader.load();

        ((ProductOverviewController) loader.getController()).setMain(this);

       File file = getProductFile();
        if (file != null) {
            loadProductFromFile(file);
        }

        return root;
    }

    private Parent loadStockOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/StockOverviewView.fxml"));
        Parent root = loader.load();

        ((StockViewController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadSaleOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/SaleOverviewView.fxml"));
        Parent root = loader.load();

        ((SaleOverviewController) loader.getController()).setMain(this);

        File file = getSaleFile();
        if (file != null) {
            loadSaleFromFile(file);
        }

        return root;
    }


    public boolean handleLogin(String text, String passwordFieldText) {


        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadProductOverviewView());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }


    public void handleLogout() {
        // clear data
        productData.clear();

        try {
            setScene(loadLoginView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleProductsView() throws IOException {
        mainView.setCenter(loadProductOverviewView());
        setScene(mainView);
    }

    public void handleStocksView() throws IOException {
        mainView.setCenter(loadStockOverviewView());
        setScene(mainView);
    }

    public void handleSalesView() throws IOException {
        mainView.setCenter(loadSaleOverviewView());
        setScene(mainView);
    }

    // Methods for Archives XML

    public File getProductFile() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String path = prefs.get("file", null);

        if (path != null) {
            return new File(path);
        } else {
            return null;
        }
    }

    public void setProductFile(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);

        if (file != null) {
            prefs.put("file", file.getPath());
            primaryStage.setTitle(title + " - " + file.getPath());
        } else {
            prefs.remove("file");
            primaryStage.setTitle(title);
        }
    }

    public void loadProductFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(ProductListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            ProductListWrapper wrapper = (ProductListWrapper) um.unmarshal(file);

            productData.clear();
            productData.addAll(wrapper.getProducts());

            primaryStage.setTitle(title + " - " + file.getPath());

            setProductFile(file);

        } catch (JAXBException e) {
            file = null;
            setProductFile(null);
        }
    }

   public void saveProductToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(ProductListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            ProductListWrapper wrapper = new ProductListWrapper();

            wrapper.setProducts(productData);

            m.marshal(wrapper, file);

            setProductFile(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void loadStockFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(Stock.class);
            Unmarshaller um = context.createUnmarshaller();

            Stock wrapper = (Stock) um.unmarshal(file);

            stockData.clear();
            stockData.addAll(wrapper);

            primaryStage.setTitle(title + " - " + file.getPath());

            setStockFile(file);

        } catch (JAXBException e) {
            file = null;
            setStockFile(null);
        }
    }

    public void saveStockToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(Stock.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            Stock wrapper = new Stock(stockData.get(0).getId(),stockData.get(0).getName(),stockData.get(0).getProductsStock());
            System.out.println(wrapper.getId() + wrapper.getName() + wrapper.getProductsStock());

            m.marshal(wrapper, file);

            setStockFile(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public File getStockFile() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String path = prefs.get("file", null);

        if (path != null) {
            return new File(path);
        } else {
            return null;
        }
    }

    public void setStockFile(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);

        if (file != null) {
            prefs.put("file", file.getPath());
            primaryStage.setTitle(title + " - " + file.getPath());
        } else {
            prefs.remove("file");
            primaryStage.setTitle(title);
        }
    }

    public File getSaleFile() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String path = prefs.get("file", null);

        if (path != null) {
            return new File(path);
        } else {
            return null;
        }
    }

    public void setSaleFile(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);

        if (file != null) {
            prefs.put("file", file.getPath());
            primaryStage.setTitle(title + " - " + file.getPath());
        } else {
            prefs.remove("file");
            primaryStage.setTitle(title);
        }
    }

    public void loadSaleFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(SaleListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            SaleListWrapper wrapper = (SaleListWrapper) um.unmarshal(file);

            saleData.clear();
            saleData.addAll(wrapper.getSales());

            primaryStage.setTitle(title + " - " + file.getPath());

            setSaleFile(file);

        } catch (JAXBException e) {
            file = null;
            setSaleFile(null);
        }
    }

    public void saveSaleToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(SaleListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            SaleListWrapper wrapper = new SaleListWrapper();

            wrapper.setSales(saleData);

            m.marshal(wrapper, file);

            setSaleFile(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    // getters

    public ObservableList<Stock> getStockData() {return stockData;}

    public void setStockData(ObservableList<Stock> stockData) {this.stockData = stockData;}

    public Stock getStock() {return getStockData().get(0);}

    public ItemStock getItemStock(int i) {return getItemStockData().get(i);};

    public ObservableList<Sale> getSaleData() {return saleData;}

    public void setSaleData(ObservableList<Sale> saleData) {this.saleData = saleData;}

    public void setItemStockData(ObservableList<ItemStock> itemStockData) {this.itemStockData = itemStockData;}

    public ObservableList<ItemStock> getItemStockData() {return itemStockData;}

    public ObservableList<Product> getProductData() {
        return productData;
    }

    public void setProductData(ObservableList<Product> productData) {
        this.productData = productData;
    }

}
