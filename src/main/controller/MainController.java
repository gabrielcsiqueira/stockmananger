package main.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import main.Main;
import main.model.Product;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Main main;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    private void handleLogout() {
        main.handleLogout();
    }

    @FXML
    private void handleClose() {
        Platform.exit();
    }

    @FXML
    private void handleProductsView() throws IOException { main.handleProductsView();}
    @FXML
    private void handleSalesView() throws IOException {main.handleSalesView();}
    @FXML
    private void handleStocksView() throws IOException {main.handleStocksView();}

    @FXML
    private void handleNew() {
        main.getProductData().clear();
        main.getSaleData().clear();
        main.getStockData().clear();
        main.setProductFile(null);
        main.setSaleFile(null);
        main.setStockFile(null);
    }

    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            main.loadProductFromFile(file);
            main.loadSaleFromFile(file);
            main.loadStockFromFile(file);
        }
    }

    @FXML
    private void handleSave() {
        File file = main.getStockFile();
        File file1 = main.getSaleFile();
        File file2 = main.getStockFile();

        if (file != null) {
            main.saveStockToFile(file2);
            main.saveProductToFile(file);
            main.saveSaleToFile(file1);
        } else {
            handleSaveAs();
        }
    }

    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showOpenDialog(null);
        File file1 = fileChooser.showOpenDialog(null);
        File file2 = fileChooser.showOpenDialog(null);
        if (file != null) {
            main.saveStockToFile(file);
            main.saveSaleToFile(file1);
            main.saveProductToFile(file2);
        }
    }

    @FXML
    private void handleAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Stock Mananger");
        alert.setHeaderText("Sobre o Stock Mananger");
        alert.setContentText("Versão 1.0 - criado por Gabriel Costa Siqueira");

        alert.showAndWait();
    }

    @FXML
    private void handleDeleteProducts(){

    }

    @FXML
    private void handleDeleteSales(){

    }
}
