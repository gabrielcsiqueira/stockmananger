package main.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import main.model.ItemStock;
import main.model.Product;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ProductEditDialogController implements Initializable {

    @FXML
    TextField idProd;
    @FXML
    TextField description;
    @FXML
    TextField price;
    @FXML
    TextField quantity;

    private Product mProduct;
    private ItemStock itemStock;

    private boolean mClicked = false;
    private Stage mWindow;

    public static boolean display(Product p, ItemStock i) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/fxml/ProductEditDialog.fxml"));
            Parent node = loader.load();
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            Scene scene = new Scene(node);
            window.setScene(scene);
            ProductEditDialogController controller = loader.getController();
            controller.setProduct(p,i);
            controller.setStage(window);
            window.showAndWait();
            return controller.mClicked;
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return false;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) { }

    public void setProduct(Product p, ItemStock i) {
        mProduct = p;
        itemStock = i;
        idProd.setText(Integer.toString(p.getId()));
        description.setText(p.getDescription());
        price.setText(Double.toString(p.getPrice()));
        quantity.setText(Integer.toString(itemStock.getQuantity())); }

    @FXML
    public void handleCancel() {
        mClicked = false;
        mWindow.close();
    }

    @FXML
    public void handleOk() {
        mProduct.setDescription(description.getText());
        mProduct.setId(Integer.parseInt(idProd.getText()));
        mProduct.setPrice(Double.parseDouble(price.getText()));
        itemStock.setIdProduct(mProduct.getId());
        itemStock.setNameProduct(mProduct.getDescription());
        itemStock.setQuantity(Integer.parseInt(quantity.getText()));

        mClicked = true;

        mWindow.close();
    }

    public void setStage(Stage window) {
        this.mWindow = window;
    }
}