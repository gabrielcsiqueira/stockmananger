package main.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.Main;
import main.model.ItemStock;
import main.model.Product;
import main.model.Stock;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductOverviewController implements Initializable {
    Main main;

    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Product, String> descriptionColumn;
    @FXML
    private TableColumn<Product, Number> idColumn;
    @FXML
    private TableColumn<Product, Number> priceColumn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        descriptionColumn.setCellValueFactory(elt -> elt.getValue().descriptionProperty());
        idColumn.setCellValueFactory(elt -> elt.getValue().idProperty());
        priceColumn.setCellValueFactory(elt -> elt.getValue().priceProperty());
    }

    public void setMain(Main main){
        this.main = main;
        productTable.setItems(main.getProductData());
    }

    @FXML
    private void handleDelete(){
        int selectedIndex = productTable.getSelectionModel().getSelectedIndex();

        if(selectedIndex >= 0 && selectedIndex < productTable.getItems().size()){
            productTable.getItems().remove(selectedIndex);
        }else {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Produto Invalido");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleNew(){
        Product p = new Product(0,"",0);
        ItemStock i = new ItemStock(0,0,"");
        Stock s = main.getStock();
        s.getProductsStock().add(i);

        boolean clicked = ProductEditDialogController.display(p,i);

        if(clicked)
        {
            main.getProductData().add(p);
            main.getItemStockData().add(i);
        }
    }

    @FXML
    private void handleEdit() {
        int selectedIndex = productTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0 && selectedIndex < productTable.getItems().size()) {

            Product p = main.getProductData().get(selectedIndex);
            ItemStock i = main.getItemStock(p.getId());

            boolean clicked = ProductEditDialogController.display(p,i);

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Nenhuma seleção");
            alert.setHeaderText("Nenhum Produto Selecionado");
            alert.setContentText("Por favor, selecione um produto.");

            alert.showAndWait();
        }
    }

}






















