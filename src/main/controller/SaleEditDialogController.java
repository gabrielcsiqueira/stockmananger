package main.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import main.model.Product;
import main.model.Sale;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SaleEditDialogController implements Initializable {

    @FXML
    TextField dateText;
    @FXML
    TextField idSale;
    @FXML
    TextField idProd;
    @FXML
    TextField quantity;

    private Sale mSale;
    private boolean mClicked = false;
    private Stage mWindow;
    private ObservableList<Product> products = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public static boolean display(Sale s, ObservableList<Product> produtos) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/fxml/SaleEditDialog.fxml"));
            Parent node = loader.load();
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            Scene scene = new Scene(node);
            window.setScene(scene);
            SaleEditDialogController controller = loader.getController();
            controller.products.addAll(produtos);
            controller.setSale(s);
            controller.setStage(window);
            window.showAndWait();
            return controller.mClicked;
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return false;
    }

    public void setSale(Sale s){
        mSale = s;
        idSale.setText(Integer.toString(s.getId()));
        dateText.setText(s.getDataSale());
        idProd.setText(Integer.toString(s.getIdProduto()));
        quantity.setText(Double.toString(s.getQuantity()));

    }

    @FXML
    private void handleCancel(){
        mClicked = false;
        mWindow.close();
    }

    @FXML
    private void handleOk(){
        for(Product p: products){
            int x = Integer.parseInt(idProd.getText());

            if(p.getId().equals(x)){
                mSale.setId(Integer.parseInt(idSale.getText()));
                mSale.setDateSale(dateText.getText());
                mSale.setIdProduct(Integer.parseInt(idProd.getText()));
                mSale.setQuantity(Double.parseDouble(quantity.getText()));
                mSale.setPriceProduct(p.getPrice());
                double a = Double.parseDouble(quantity.getText());
                double b = p.getPrice();
                double valor = a*b;
                mSale.setTotalPrice(valor);
                mClicked = true;
                mWindow.close();
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Nenhuma seleção");
                alert.setHeaderText("Nenhum Produto Selecionado");
                alert.setContentText("Por favor, insira o codigo do produto.");

                alert.showAndWait();
            }
        }
    }

    public void setStage(Stage window) {
        this.mWindow = window;
    }
}
