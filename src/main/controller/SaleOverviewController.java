package main.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import main.Main;
import main.model.Product;
import main.model.Sale;

import java.net.URL;
import java.util.ResourceBundle;

public class SaleOverviewController implements Initializable {
    Main main;

    @FXML
    private TableView<Sale> salesTable;
    @FXML
    private TableColumn<Sale, String> dataSaleColumn;
    @FXML
    private TableColumn<Sale, Number> totalPrice;
    @FXML
    private TableColumn<Sale, Number> idColumn;
    @FXML
    private TableColumn<Sale, Number> productsColumn;

    public SaleOverviewController() {
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dataSaleColumn.setCellValueFactory(elt -> elt.getValue().dateSaleProperty());
        idColumn.setCellValueFactory(elt -> elt.getValue().idProperty());
        totalPrice.setCellValueFactory(elt -> elt.getValue().totalPriceProperty());
        productsColumn.setCellValueFactory(elt -> elt.getValue().idProductProperty());
    }

    public void setMain(Main main){
        this.main = main;
        salesTable.setItems(main.getSaleData());
    }

    @FXML
    private void handleNew(){
        Sale s = new Sale(0,0,"",0,0,0);
        ObservableList<Product> products = main.getProductData();
        boolean clicked = SaleEditDialogController.display(s,products);

        if(clicked){
            main.getSaleData().add(s);
        }

    }

    @FXML
    private void handleEdit(){

    }

    @FXML
    private void handleDelete(){
        int selectedIndex = salesTable.getSelectionModel().getSelectedIndex();

        if(selectedIndex >= 0 && selectedIndex < salesTable.getItems().size()){
            salesTable.getItems().remove(selectedIndex);
        }else {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Produto Invalido");
            alert.showAndWait();
        }
    }

}
