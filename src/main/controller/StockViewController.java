package main.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.Main;
import main.model.ItemStock;
import main.model.Product;

import java.net.URL;
import java.util.ResourceBundle;

public class StockViewController implements Initializable {
    Main main;

    @FXML
    private TableView<ItemStock> stockTable;
    @FXML
    private TableColumn<ItemStock, Number> idProductColumn;
    @FXML
    private TableColumn<ItemStock, String> nameProductColumn;
    @FXML
    private TableColumn<ItemStock, Number> quantityColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idProductColumn.setCellValueFactory(elt -> elt.getValue().idProductProperty());
        nameProductColumn.setCellValueFactory(elt -> elt.getValue().nameProductProperty());
        quantityColumn.setCellValueFactory(elt -> elt.getValue().quantityProperty());
    }

    public void setMain(Main main) {
        this.main = main;
        stockTable.setItems(main.getStockData().get(0).getProductsStock());


    }
}