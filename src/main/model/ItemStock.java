package main.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemStock {

    private final IntegerProperty idProduct;
    private  final StringProperty nameProduct;
    private final IntegerProperty quantity;

    public ItemStock(){ this (0,0,"");}

    public ItemStock(int idProduct, int quantity, String nameProduct){
        this.idProduct = new SimpleIntegerProperty(idProduct);
        this.quantity =  new SimpleIntegerProperty(quantity);
        this.nameProduct = new SimpleStringProperty(nameProduct);
    }

    public void setIdProduct(int idProduct){this.idProduct.set(idProduct); }

    public IntegerProperty idProductProperty() {return idProduct;}

    public int getIdProduct(){return idProduct.get();}

    public void setQuantity(int quantity){this.quantity.set(quantity);}

    public IntegerProperty quantityProperty() { return quantity; }

    public int getQuantity(){ return quantity.get();}

    public void setNameProduct(String nameProduct) { this.nameProduct.set(nameProduct);}

    public StringProperty nameProductProperty() { return nameProduct; }

    public String getNameProduct(){ return nameProduct.get();}


}
