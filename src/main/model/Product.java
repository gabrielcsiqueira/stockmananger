package main.model;

import javafx.beans.property.*;

public class Product {

    private final IntegerProperty id;

    private final StringProperty description;

    private final DoubleProperty price;

    public Product() {
        this(0, null, 0);
    }

    public Product(Integer id, String description, double price) {

        this.id = new SimpleIntegerProperty(id);
        this.description = new SimpleStringProperty(description);
        this.price = new SimpleDoubleProperty(price);
    }

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id.setValue(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.setValue(price);
    }

    public DoubleProperty priceProperty() {
        return price;
    }

}