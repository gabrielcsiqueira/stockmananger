package main.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.util.List;

public class Sale {

    private final IntegerProperty id;
    private final  StringProperty dateSale;
    private final IntegerProperty idProduct;
    private final DoubleProperty quantity;
    private final DoubleProperty priceProduct;
    private final DoubleProperty totalPrice;

    public Sale(){
        this(0,0,"", 0, 0,0);
    }

    public Sale(int id, int idProduct,String dataSale, double quantity, double priceProd, double totalPrice){
        this.id = new SimpleIntegerProperty(id);
        this.idProduct = new SimpleIntegerProperty(idProduct);
        this.dateSale = new SimpleStringProperty(dataSale);
        this.quantity = new SimpleDoubleProperty(quantity);
        this.priceProduct = new SimpleDoubleProperty(priceProd);
        this.totalPrice = new SimpleDoubleProperty(totalPrice);

    }

    public IntegerProperty idProperty() {
        return id;
    }

    public Integer getId(){
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public  void setDateSale(String dataSale){
        this.dateSale.set(dataSale);
    }

    public String getDataSale(){
        return dateSale.get();
    }

    public StringProperty dateSaleProperty() {
        return dateSale;
    }

    public void setIdProduct(int idProduct){this.idProduct.set(idProduct);}

    public void setQuantity(double quantity) { this.quantity.set(quantity);}

    public void setPriceProduct(double priceProduct) {this.priceProduct.set(priceProduct);}

    public IntegerProperty idProductProperty() {return idProduct;}

    public DoubleProperty quantityProperty() { return quantity; }

    public DoubleProperty priceProductProperty() { return priceProduct; }

    public int getIdProduto() { return idProduct.get(); }

    public double getQuantity() { return quantity.get(); }

    public double getPriceProduct() {return priceProduct.get();}

    public void setTotalPrice(double valor) {this.totalPrice.set(valor);}

    public double getTotalPrice() {return totalPrice.get(); }

    public DoubleProperty totalPriceProperty() { return totalPrice;}
}
