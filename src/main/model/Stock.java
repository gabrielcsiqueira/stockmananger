package main.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "Stock")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Stock", propOrder = {"id", "name", "productsStock"})
public class Stock {

    @XmlElement(name = "id", required = true)
    private IntegerProperty id;
    @XmlElement(name = "name", required = true)
    private StringProperty name;
    @XmlElementWrapper(name = "productsStock")
    @XmlElement(name = "product")
    private ObservableList<ItemStock> productsStock;

    public Stock(int id,String name,List<ItemStock> productList) {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.productsStock = FXCollections.observableArrayList(productList);
    }

    public Stock(){
        this(0,null,null);
    }


    public IntegerProperty getIdProperty() {
        return id;
    }

    public Integer getId(){ return id.get(); }

    public void setId(IntegerProperty id) {
        this.id = id;
    }

    public ObservableList<ItemStock> getProductsStock(){
        return productsStock;
    }

    public void setName(String name){
        this.name.set(name);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty(){
        return name;
    }
}
